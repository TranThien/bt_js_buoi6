// function show() {
//     soChan = 0;
//     soLe = 0;
//     for (var step = 0; step < 100; step++) {
//         if (step % 2 == 0) {
//             soChan = soChan + step + " ";
//         } else {
//             soLe = soLe + step + " ";
//         }
//     }

//     console.log("soChan:", soChan);
//     console.log("soLe:", soLe);
// }

// Bài 1
function show() {
    var sum = 0;
    var current = 0;
    for (var step = 0; step < 10000; step++) {
        sum = sum + step;
        current = step;
        if (sum > 10000) {
            break;
        }
    }
    // console.log("step:", step);
    // console.log("current", current);
    // console.log(sum);

    document.getElementById(
        "result"
    ).innerHTML = `Số nguyên dương nhỏ nhất : ${current}`;
    document.getElementById("result").classList.add("text-success");
}

// Bài 2
function tinhTong() {
    var number1Value = document.getElementById("txt-number-1").value * 1;
    var number2Value = document.getElementById("txt-number-2").value * 1;
    var output = 0;
    for (var i = 1; i <= number2Value; i++) {
        var total = number1Value ** i;

        output += total;
    }
    document.getElementById("content").innerHTML = `Tổng : ${output}`;
}
// Bài 3
function tinhGiaiThua() {
    var soThuNhat = document.getElementById("txt-so-thu-nhat").value * 1;
    var result = 1;

    for (var i = 1; i <= soThuNhat; i++) {
        result = result * i;
    }
    // var step = 1;
    // while (step <= soThuNhat) {
    //     result = result * step;

    //     step++;
    // }
    // do {
    //     result = result * step;
    //     step++;
    // } while (step <= soThuNhat);
    document.getElementById("text").innerHTML = result;
}

// Bài 4

function showDiv() {
    var nhapSo = document.getElementById("txt-nhap-so").value * 1;
    var divChan = 0;
    var divLe = 0;
    var output = "";
    for (var i = 1; i <= nhapSo; i++) {
        if (i % 2 == 0) {
            divChan = i;
            var div = `<div class="btn-success text-warning"> ${divChan} </div>`;
            // continue;
        } else {
            divLe = i;
            var div = `<div class="btn-danger text-info"> ${divLe} </div>`;
            // continue;
        }
        output += div;
    }
    document.getElementById("container").innerHTML = output;
}